(function($){

	// create a unique id
	function uniqid (prefix, more_entropy) {
		// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		// +    revised by: Kankrelune (http://www.webfaktory.info/)
		// %        note 1: Uses an internal counter (in php_js global) to avoid collision
		// *     example 1: uniqid();
		// *     returns 1: 'a30285b160c14'
		// *     example 2: uniqid('foo');
		// *     returns 2: 'fooa30285b1cd361'
		// *     example 3: uniqid('bar', true);
		// *     returns 3: 'bara20285b23dfd1.31879087'
		if (typeof prefix == 'undefined') {
			prefix = "";
		}

		var retId;
		var formatSeed = function (seed, reqWidth) {
			seed = parseInt(seed, 10).toString(16); // to hex str
			if (reqWidth < seed.length) { // so long we split
				return seed.slice(seed.length - reqWidth);
			}
			if (reqWidth > seed.length) { // so short we pad
				return Array(1 + (reqWidth - seed.length)).join('0') + seed;
			}
			return seed;
		};

		// BEGIN REDUNDANT
		if (!this.php_js) {
			this.php_js = {};
		}
		// END REDUNDANT
		if (!this.php_js.uniqidSeed) { // init seed with big random int
			this.php_js.uniqidSeed = Math.floor(Math.random() * 0x75bcd15);
		}
		this.php_js.uniqidSeed++;

		retId = prefix; // start with prefix, add current milliseconds hex string
		retId += formatSeed(parseInt(new Date().getTime() / 1000, 10), 8);
		retId += formatSeed(this.php_js.uniqidSeed, 5); // add seed hex string
		if (more_entropy) {
			// for more entropy we add a float lower to 10
			retId += (Math.random() * 10).toFixed(8).toString();
		}

		return retId;
	}


    function count_tabs() {

        $tabarea    = $(".meta-box-sortables");
        $tabs       = $tabarea.find('.umt-tab').length;

        if ( $tabs == 0 ) {
            $(".umt-no-tabs").show();
        } else if ( $tabs > 0 ) {
            $(".umt-no-tabs").hide();
        }

    }


    /**
     * Delete a metabox row from a tab group
     */
    $("a.umt-delete-tab__row").live( 'click', function() {

        event.preventDefault ? event.preventDefault() : event.returnValue = false;

        // Match data id's before removing the row
        if( $(this).data('row-id') == $(this).parent().parent().data('row-id') ) {

            $(this).parent().parent().remove();

        }

    });


    /**
     * Add a metabox row to a tab group
     */
    $("button.umt-add-tab__row").live( 'click', function() {

        event.preventDefault ? event.preventDefault() : event.returnValue = false;

        var $groupID    = $(this).parent().parent().parent().parent().parent();
        var groupID     = $groupID.attr('group-id');


		if (groupID !== undefined) {

			var newDivID = $('#new-row div.tab-box-row').clone();
            var newId    = uniqid();

            $(newDivID).find('input.metabox-divid').attr('name','umt_div[' + groupID +'][' + newId + ']');
            $(newDivID).attr('data-row-id', newId );
            $(newDivID).find('a.umt-delete-tab__row').attr('data-row-id', newId );
			$($groupID).find('.umt-tab-metaboxes').append( $(newDivID) );
		}

    });


    /**
     * Update the input value woth the selected metabox id
     */
	$('.post_divselect').live('change', function(event){
		$(this).parent().parent().find('input.metabox-divid').val( $(this).val() );
	});


    /**
     * Update the tab title when the input content changes
     */
	$('input.group_name_input').live('change', function(event){
		$(this).parent().parent().parent().parent().find('span.js-tab-title').text( $(this).val() );
	});


    /**
     * Remove tab box
     */
	$('.umt-delete-tab').live('click', function(event){

		event.preventDefault ? event.preventDefault() : event.returnValue = false;

		$(this).parent().parent().remove();

        count_tabs();

	});


    /**
     * Create new tab box
     */
    $('.umt-add-tab').live('click', function(event){
        event.preventDefault ? event.preventDefault() : event.returnValue = false;

        var newGroup    = $('#new-tab .umt-tab').clone()
        var newGroupID  = uniqid();

        $(newGroup).attr( 'group-id', newGroupID) ;

        $(newGroup).find('input.group_name_input').attr('name','umt_group[' + newGroupID +']');

        $(this).before($(newGroup));

        // add new div
        var groupID = newGroupID;
        if (groupID !== undefined) {

            var newDivID = $('#new-row div.tab-box-row').clone()

            // $(newDivID).children('input').attr('name','umt_div[' + groupID +'][' + uniqid() + ']');

            $(newGroup).appendTo($('.meta-box-sortables'));

        }

        count_tabs();

    });





    // Make things sortable
	$(document).ready(function() {

        count_tabs();

        // Make rows draggable
		$('.meta-box-sortables').sortable({ handle: $(this).find('h2.ui-sortable-handle'), placeholder: 'meta-box-sortables', forcePlaceholderSize: true });

        // Make Tabs draggable
		$('.umt-tab-metaboxes').sortable({ handle: "a.move-tab", placeholder: 'umt-ui-state-highlight', forcePlaceholderSize: true });

	});



    // Create new tab box
	$('.metabox-newgroup').live('click', function(event){
		event.preventDefault ? event.preventDefault() : event.returnValue = false;
		var newGroup = $('#meta-newgroup .group').clone()
		var newGroupID = uniqid();
		$(newGroup).attr('group-id',newGroupID);
		$(newGroup).children('div').children('table').children('tbody').children('tr').children('td').children('div').children('input').attr('name','umt_group[' + newGroupID +']');
		$(this).before($(newGroup));

		// add new div
		var groupID = newGroupID;
		if (groupID !== undefined)
		{
			var newDivID = $('#meta-newdiv .div').clone()
			$(newDivID).children('input').attr('name','umt_div[' + groupID +'][' + uniqid() + ']');
			$(newGroup).children('ul').append($(newDivID));
		}
	});



    // Create new grouop row
	$('.metabox-newdiv').live('click', function(event){
		event.preventDefault ? event.preventDefault() : event.returnValue = false;
		var groupID = $(this).parent().parent().parent().attr('group-id');
		if (groupID !== undefined)
		{
			var newDivID = $('#meta-newdiv .div').clone()
			$(newDivID).children('table').children('tbody').children('tr').children('td').children('div').children('input').attr('name','umt_div[' + groupID +'][' + uniqid() + ']');
			$(this).parent().parent().parent().children('.information').children('ul').append($(newDivID));
		}
	});



    // Remove tab box
	$('.metabox-groupremove').live('click', function(event){
		event.preventDefault ? event.preventDefault() : event.returnValue = false;
		$(this).parent().parent().parent().parent().parent().parent().remove();
	});


    // Remove group row
	$('.metabox-divremove').live('click', function(event){
		event.preventDefault ? event.preventDefault() : event.returnValue = false;
		$(this).parent().parent().parent().parent().parent().parent().remove();
	});


    // Update select
	$('.metabox-divid').live('change', function(event){
		$( this )
        .parent( )
        .children( 'select' )
        .val(
            $(this).val( )
        );
	});


    // Update input
	$('.post_divselect').live('change', function(event){
		$(this).parent().children('input').val( $(this).val() );
	});


    // Make things sortable
	$(document).ready(function() {

        // Make rows draggable
		$('.div_sort').sortable({ handle: $(this), placeholder: 'ui-state-highlight', forcePlaceholderSize: true });

        // Make Tabs draggable
		$('.meta-group').sortable({ handle: "table thead", placeholder: 'ui-state-highlight', forcePlaceholderSize: true });

	});







})(jQuery);
