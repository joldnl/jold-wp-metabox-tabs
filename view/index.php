<?php

$post_types = get_post_types('','objects');
$load_pages = array();

$args = array(
    "post_type"         => "page",
    "posts_per_page"    => -1,
);


// Check if Polylang is active
if( function_exists( 'pll_default_language' ) ) {

    $current_lang = pll_default_language();

    if( !empty( $current_lang ) ) {

        $args['lang'] = $current_lang;

    }
}

// Get posts
$pages = get_posts( $args );

if( $pages ) {
    foreach ( $pages as $page ) {

        $page_id = $page->ID;

        if( function_exists('pll_get_post') ) {

            $lang_id = pll_get_post( $page->ID, pll_default_language() );

            if( $lang_id == $page_id )
                $load_pages[] = $page;

        } else {
            $load_pages[] = $page;
        }
    }
}
?>


<div class="wrap">

    <?php include( "page-header.php" ); ?>

    <p> </p>

    <table class="wp-list-table widefat fixed striped posts" cellspacing="0">
        <thead>
            <tr>
                <td class="manage-column column-cb check-column"><input type="checkbox" disabled /></td>
                <th scope="col" id="title" class="manage-column column-title column-primary desc"><?php echo __('Title','jold-metabox-tabs'); ?></th>
                <th scope="col" id="date" class="manage-column column-author desc"><?php echo __('Slug','jold-metabox-tabs'); ?></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <td class="manage-column column-cb check-column"><input type="checkbox" disabled /></td>
                <th scope="col" id="title" class="manage-column column-title column-primary desc"><?php echo __('Title','jold-metabox-tabs'); ?></th>
                <th scope="col" id="date" class="manage-column column-author desc"><?php echo __('Slug','jold-metabox-tabs'); ?></th>
            </tr>
        </tfoot>

        <tbody id="the-list">

            <?php $posttype_url = add_query_arg('options', '1', $this->menu_url); ?>

            <tr valign="top">
                <th scope="row" class="check-column"><input type="checkbox" disabled /></th>
                <td class="post-title page-title column-title">
                    <strong><a class="row-title" href="<?php echo $posttype_url; ?>" title="Edit"><?php echo __('Global Options','jold-metabox-tabs'); ?></a></strong>
                </td>
                <td><?php //echo $post_type->name; ?></td>
            </tr>


            <?php foreach ($this->settings_pages as $slug => $settings_page ): ?>

                <?php $posttype_url = add_query_arg('settings', $slug, $this->menu_url); ?>

                <tr valign="top">
                    <th scope="row" class="check-column"><input type="checkbox" disabled /></th>
                    <td class="post-title page-title column-title">
                        <strong><a class="row-title" href="<?php echo $posttype_url; ?>" title="Edit"><?php echo $settings_page['name']; ?></a></strong>
                    </td>
                    <td class="date-title column-author"><?php echo $slug; ?></td>
                </tr>
            <?php endforeach; ?>


            <?php foreach ( $post_types as $post_type ): ?>

                <?php if ($this->post_types_ignored($post_type->name)) { continue; } ?>
                <?php $posttype_url = add_query_arg('posttype', $post_type->name, $this->menu_url); ?>

                <tr valign="top">
                    <th scope="row" class="check-column"><input type="checkbox" disabled /></th>
                    <td class="post-title page-title column-title">
                        <strong>
                            <a class="row-title" href="<?php echo $posttype_url; ?>" title="Edit"><?php echo $post_type->label; ?></a>
                        </strong>
                    </td>
                    <td class="date-title column-author"><?php echo $post_type->name; ?></td>
                </tr>
            <?php endforeach; ?>


            <?php foreach ($load_pages as $page ): ?>
                <?php $page_url = add_query_arg('page_id', $page->ID, $this->menu_url); ?>

                <tr valign="top">
                    <th scope="row" class="check-column"><input type="checkbox" disabled /></th>
                    <td class="post-title page-title column-title">
                        <strong>
                            <a class="row-title" href="<?php echo $page_url; ?>" title="Edit"><?php echo $page->post_title; ?></a>
                        </strong>
                    </td>
                    <td class="date-title column-author"><?php echo $page->post_type; ?></td>
                </tr>
            <?php endforeach; ?>

        </tbody>

    </table>

</div>
