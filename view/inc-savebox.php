<div id="submitdiv" class="postbox">
    <h2><span><?php _e( 'Save Tabs', 'jold-metabox-tabs' ); ?></span></h2>
    <div class="inside">
        <div id="major-publishing-actions">
            <div id="delete-action">
                <a class="submitdelete deletion" href="<?php echo $this->menu_url; ?>"><?php _e( 'Go back to overview', 'jold-metabox-tabs' ); ?></a>
            </div>
            <div id="publishing-action">
                <input name="updateoption" type="submit" class="button button-primary button-large" tabindex="5" accesskey="p" value="<?php _e('Update', 'jold-metabox-tabs'); ?>">
                <?php echo wp_nonce_field( -1, "_wpnonce", true ,false ); ?>
                <input type="hidden" name="umt_sent" value="1">
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
