<div class="postbox umt-tab" group-id="<?php echo $group['id']; ?>">

    <h2 class="hndle ui-sortable-handle">
        <span><?php _e( 'Tab', 'jold-metabox-tabs' ); ?>: <span class="js-tab-title"><?php echo $group['name']; ?></span></span>
        <a href="#" class="dashicons dashicons-no umt-delete-tab" title="Edit field group"></a>
    </h2>


    <div class="umt-inner-box">

        <div class="umt-row">
            <div class="umt-column-left">
                <strong><?php _e( 'Tab title', 'jold-metabox-tabs' ); ?>:</strong>
            </div>
            <div class="umt-column-right">
                <input class="group_name_input" type="text" name="umt_group[<?php echo $group['id']; ?>]" size="30" tabindex="1" value="<?php echo $group['name']; ?>" id="title" autocomplete="off">
            </div>
        </div>

        <div class="umt-row">

            <div class="umt-column-left">
                <strong><?php _e( 'Metaboxes', 'jold-metabox-tabs' ); ?>:</strong>
            </div>

            <div class="umt-column-right">
                <div class="umt-tab-metaboxes umt-ui-state-highlight">
                    <?php if( isset( $group['div'] ) ): ?>
            			<?php foreach( $group['div'] as $div ): ?>
                            <?php umt_get_rows( $umt, $div, $group, $div_options ); ?>
            			<?php endforeach; ?>
        			<?php endif; ?>
                </div>

                <div class="umt-tab-metaboxes--add">
                    <button type="button" name="button" class="btn btn-small button button-small button umt-add-tab__row"><?php _e( 'Add metabox', 'jold-metabox-tabs' ); ?></button>
                </div>


            </div>

        </div>
    </div>
</div>
