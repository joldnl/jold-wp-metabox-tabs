<?php

if ( isset($_REQUEST['options']) ) {
	$post_info = array();
	$post_name = __('Global Options','jold-metabox-tabs');
}
else if ( isset($_REQUEST['settings']) ) {
	$post_info = array();
	$post_name = $this->extensions[$_REQUEST['settings']]['name'];
}
else if ( isset($_REQUEST['page_id']) ) {
	$post_info = get_post($_REQUEST['page_id'] );
	$post_name = __('Page ') . $post_info->post_title;
}
else {
	$post_info = get_post_type_object($this->post_type);
	$post_name = $post_info->labels->name;
}


function umt_edit_title( $request ) {}

function umt_get_tab( $umt, $group, $div_options ) {
    include( 'inc-tab.php' );
}

function umt_get_rows( $umt, $div, $group, $div_options ) {
    include( 'inc-rows.php' );
}

?>

<div id="metabox-editor" class="wrap">

    <h1 class="wp-heading-inline"><?php _e('Jold Metabox Tabs','jold-metabox-tabs'); ?> - <?php echo $post_name; ?></h1>
    <!-- <a class="page-title-action">v<?php echo $this->version; ?></a> -->
    <p></p>

	<?php if ($save > 0): ?>
    	<div id="message" class="updated below-h2">
    		<p><?php echo __("Saved successfully.","jold-metabox-tabs"); ?></p>
    	</div>
	<?php endif; ?>

	<div id="meta-data" style="display:none;">
		<div id="new-tab">
			<?php umt_get_tab( $this, array('name' => '', 'id' => uniqid()), $this->div_options ); ?>
		</div>
		<div id="new-row">
			<?php umt_get_rows( $this, array('name' => '', 'id' => uniqid()), array('id' => uniqid()), $this->div_options ); ?>
		</div>
	</div>

    <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-2">
            <div id="post-body-content">

                <form action="#" method="POST">

                    <div id="postbox-container-1" class="postbox-container">
                        <?php include( 'inc-savebox.php' ); ?>
                    </div>

                    <div id="postbox-container-2" class="postbox-container">
                        <div id="normal-sortables" class="meta-box-sortables">

                            <?php if ( empty( $groups) ): ?>
                                <div class="umt-no-tabs"><?php _e( 'You have no tabs yet.', 'jold-metabox-tabs' ); ?></div>
                            <?php endif; ?>

                            <?php foreach( $groups as $group ): ?>
    			                <?php umt_get_tab( $this, $group, $this->div_options ); ?>
            				<?php endforeach; ?>

                        </div>
                        <div class="umt-submit-conainer" style="text-align: right; padding: 0;">
                            <button type="button" name="button" id="add-tab-button" class="button button-primary button-large umt-add-tab"><?php _e( 'Add new tab', 'jold-metabox-tabs' ); ?></button>
                        </div>
                    </div>

                </form>

            </div>
            <br class="clear" />
        </div>
    </div>

</div>
