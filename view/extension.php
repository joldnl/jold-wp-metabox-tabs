<div class="wrap">

    <?php include( "page-header.php" ); ?>

    <div class="extension-list">
		<div class="extension">
			<?php if (count($this->extensions)<=0) : ?>
			<h3><?php echo __('No extensions available. ACF Extensions will remain hidden unless ACF is active.','jold-metabox-tabs'); ?></h3>
			<?php endif; ?>
			<?php foreach ($this->extensions as $slug => $extension): ?>
			<?php $description_array = explode("\n",$extension['description']); ?>
			<?php $url = $extension['enabled'] ? $this->menu_url . "&disable=" . $slug : $this->menu_url . "&enable=" . $slug ; ?>
			<h3><?php echo $extension['name']; ?></h3>
			<div class="extension_description">
			<?php foreach ($description_array as $description) : ?>
			<p><?php echo $description; ?></p>
			<?php endforeach; ?>
			</div>
			<a href="<?php echo $url; ?>"><?php echo $extension['enabled'] ? 'Disable' : 'Enable' ; ?></a>
			<?php endforeach; ?>
		</div>
	</div>
</div>
