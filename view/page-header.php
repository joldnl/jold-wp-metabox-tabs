
    <h1 class="wp-heading-inline"><?php _e('Jold Metabox Tabs','jold-metabox-tabs'); ?></h1>
    <a class="page-title-action">v<?php echo $this->version; ?></a>

    <div class="clearfix"></div>

    <p></p>

    <h2 class="nav-tab-wrapper">
        <a href="<?php echo $this->menu_url; ?>" class="nav-tab<?php if( !isset($_GET["subpage"]) ) { echo ' nav-tab-active'; }; ?>"><?php _e('General','jold-metabox-tabs'); ?></a>
        <a href="<?php echo add_query_arg('subpage', 'extension' , $this->menu_url); ?>" class="nav-tab<?php if( isset($_GET["subpage"]) && $_GET["subpage"] == 'extension' ) { echo ' nav-tab-active'; }; ?>"><?php _e('Extensions','jold-metabox-tabs'); ?></a>
        <a href="<?php echo add_query_arg('subpage', 'patcher' , $this->menu_url); ?>" class="nav-tab<?php if( isset($_GET["subpage"]) && $_GET["subpage"] == 'patcher' ) { echo ' nav-tab-active'; }; ?>"><?php _e('Patches','jold-metabox-tabs'); ?></a>
    </h2>
