
<div class="tab-box-row" data-row-id="<?php echo $div['id']; ?>">
    <div class="tab-box-row--move">
        <a href="#" class="dashicons dashicons-move umt-delete-tab move-tab" title="<?php _e( 'Move this metbabox row', 'jold-metabox-tabs' ); ?>"></a>
    </div>
    <div class="tab-box-row--id">
        <input type="text" name="umt_div[<?php echo $group['id']; ?>][<?php echo $div['id']; ?>]" size="30" tabindex="1" value="<?php echo $div['name']; ?>" class="metabox-divid" autocomplete="off">
    </div>
    <div class="tab-box-row--select">
        <?php if (count($div_options)>0): ?>
            <select class="post_divselect" selected="<?php echo $div['name']; ?>">
            	<option name="none" value="">- <?php _e('None', 'jold-metabox-tabs'); ?> -</option>
            	<?php foreach ($div_options as $optiongroup): ?>
                	<optgroup label="<?php echo $optiongroup['name']; ?>">
                		<?php foreach ($optiongroup['div'] as $optiondiv): ?>
                    		<?php
                    			$class = isset($optiondiv['class']) ? ' class="' . $optiondiv["class"] . '"' : "";
                    			$selected = $div['name'] == $optiondiv['value'] ? ' selected="selected"' : '';
                    		?>
                    		<option name="<?php echo $optiondiv['value']; ?>" value="<?php echo $optiondiv['value']; ?>" <?php echo $class . $selected; ?>><?php echo $optiondiv['name']; ?></option>
                		<?php endforeach; ?>
                	</optgroup>
                <?php endforeach; ?>
            </select>
        <?php endif; ?>
    </div>
    <div class="tab-box-row--delete">
        <a href="#" class="dashicons dashicons-no umt-delete-tab umt-delete-tab__row" data-row-id="<?php echo $div['id']; ?>" title="?php _e( 'Edit field group', 'jold-metabox-tabs' ); ?>"></a>
    </div>
</div>
