<?php
/*--------------------------------------------------------------------------
*
*	umt_wpboxes_support
*	Adds the default WordPress meta boxes to Metabox Tabs
*
*	@author Jurgen Oldenburg
*
*-------------------------------------------------------------------------*/

class umt_wpboxes_support {

	var $umt, $input;

	function __construct($parent) {

		$this->umt = $parent;

		// setup initiation procedure
		add_action('init',array($this,'init'),99);
	}

	function init() {

        // hooks to the admin_men_print_styles if its used by Ultimate Metabox tabs
        add_action( 'umt_admin_menu_print_styles', array( $this, 'admin_menu_print_styles' ) );

	}

	function admin_menu_print_styles() {

		$posts     = [];
        $metaboxes = [
            'slugdiv'           => __('Slug', 'jold-metabox-tabs'),
            'revisionsdiv'      => __('Revisions', 'jold-metabox-tabs'),
            'authordiv'         => __('Author', 'jold-metabox-tabs'),
            'commentsdiv'       => __('Comments', 'jold-metabox-tabs'),
            'commentstatusdiv'  => __('Discussion', 'jold-metabox-tabs'),
            'pageparentdiv'     => __('Page Attrubites', 'jold-metabox-tabs'),
            'postcustom'        => __('Custom Fields', 'jold-metabox-tabs'),
        ];

        foreach ( $metaboxes as $id => $name ) {

            $new_post = [];
            $new_post['name']   = $name;
            $new_post['value']  = $id;

            array_push( $posts, $new_post );

        }

		// Places the posts into a div group list
		umt_register_div_types( __( 'WP Metaboxes', 'jold-metabox-tabs' ), $posts );

	}
}
?>
