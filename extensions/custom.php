<?php
/*--------------------------------------------------------------------------
*
*	umt_wpboxes_support
*	Adds the default WordPress meta boxes to Metabox Tabs
*
*	@author Jurgen Oldenburg
*
*-------------------------------------------------------------------------*/


class umt_custom_metabox_support  {

	var $umt, $input;

	function __construct($parent) {

		$this->umt = $parent;

		// Setup and initiation procedure
		add_action( 'init', array( $this, 'init' ), 99 );
	}

	function init() {

        add_action('admin_head', array( $this, 'admin_head' ) );

        // hooks to the admin_men_print_styles if its used by Ultimate Metabox tabs
        add_action( 'umt_admin_menu_print_styles', array( $this, 'admin_menu_print_styles' ) );

	}


    function admin_head() {

		$this->umt->admin_head();

		// Filter metaboxes if on Shopp page.
		add_filter( 'umt_filter_metabox_screen', array( $this, 'filter_metabox_screen' ), 10, 1 );

	}


	function admin_menu_print_styles() {

		$posts     = [];
        $metaboxes = [
            'project_votes'     => __('Project Votes', 'jold-metabox-tabs'),
        ];

        foreach ( $metaboxes as $id => $name ) {

            $new_post = [];
            $new_post['name']   = $name;
            $new_post['value']  = $id;

            array_push( $posts, $new_post );

        }

		// Places the posts into a div group list
		umt_register_div_types( __( 'Custom Metaboxes', 'jold-metabox-tabs' ), $posts );

	}



    function filter_metabox_screen( $the_wp_meta_boxes = array() ) {

		// Get the current screen
		global $wp_meta_boxes;
		$screen = get_current_screen()->id;

        // Add custom registered metaboxes
        if (isset($wp_meta_boxes[$screen]['advanced']['default'])) {

            $the_wp_meta_boxes = array_merge($the_wp_meta_boxes,$wp_meta_boxes[$screen]['advanced']['default']);

        }

		return $the_wp_meta_boxes;
	}


}
?>
