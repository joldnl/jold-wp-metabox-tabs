=== Jold: Metabox Tabs ===
Contributors: Jurgen Oldenburg
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html
Tags: WordPress, Metabox Tabs, Jold
Requires at least: 4.6
Tested up to: 4.7.3
Stable tag: 1.0.12

Adds extendable metabox tabs to your posts (and specific pages). Based on a plugin by SilbinaryWolf (Ultimate Metabox Tabs)


== Changelog ==

= 1.0.12 =

Release Date: April 22th, 2017

* Disable acf options page support


= 1.0.11 =

Release Date: June 7h, 2017

* Add custom firestarter metabox support



= 1.0.10 =

Release Date: April 22th, 2017

* Sort acf boxes alphabetically
* Make metabox names translatable
* Add Dutch translation
* Add support for default WordPress metaboxes.
  This now includes options for the following metbaoxes:
  * Slug
  * Author
  * Comments
  * Discussion
  * Revisions
  * Page Attirubites
