# Jold Metabox Tabs
Jold Metabox Tabs adds metabox tabs to your posts, pages and the Advanced Custom Fields options page. They allow you to easily allow tabbable and sortable metaboxes on your editing page so that your client can do less scrolling and more editing. They’re also inserted at server-time, meaning no ugly javascript forcing the metaboxes into the page.


## Must-use plugin
This plugin is a must-use plugin, and will automatically be activated upon installation.
