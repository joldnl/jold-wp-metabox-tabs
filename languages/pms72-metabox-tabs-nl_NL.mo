��    9      �  O   �      �     �     �            S   $  E   x  [   �  {        �  X   �  \        c     o     v       
   �     �  
   �     �     �     �     �  C   �     )  	   6     @     W  P   c     �     �     �  ?   �     	     &	     ,	     <	     J	     f	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     �	  	   �	     
     
     
     ,
     3
     @
     O
     f
  �  |
  
   e     p     �     �  z   �  k     M   �  p   �     F  i   ]  �   �     O     _     f     o  	   �     �  	   �     �     �     �     �  3   �       	   &     0     M  Z   [     �     �     �  D   �          5     <     K     Y      t     �     �     �     �     �     �                 	         *     <     B     V     ^     k     z     �         8                     6      ,         -              %   !   .   1   (         $                   *   5      )          2   #                   7   +       4           '                   3   	          9             0                 
             &   /                 "       ACF Options ACF list custom fields Add metabox Add new tab Adds SEO Framework support. So you can load SEO Framework metaboxes inside the tabs Adds WP SEO support. So you can load WP SEO metaboxes inside the tabs Adds default WordPress metaboxes. This adds the default WP metaboxes available to the tabs. Adds extendable metabox tabs to your posts (and specific pages). Based on a plugin by SilbinaryWolf (Ultimate Metabox Tabs) Advanced Custom Fields An error occurred patching. Make sure you CMOD settings for wp-admin are writeable (777) An error occurred uninstalling. Make sure you CMOD settings for wp-admin are writeable (777) Apply Patch Author Comments Custom Fields Discussion ERROR PATCHING Extensions General Global Options Go Back Go back to overview Lists ACF custom fields in a selectable list to the metatab editor. Metabox Tabs Metaboxes Move this metbabox row NOT PATCHED No extensions available. ACF Extensions will remain hidden unless ACF is active. No patches available. None OK Overrides the ACF Options page class and gives it Metabox Tabs. PMS 72 Metabox Tabs Page  Page Attrubites Patch Status: Patch applied successfully. Patch uninstalled successfully. Patches Pms72: Metabox Tabs Revisions SEO Framework Support Save Tabs Saved successfully. Slug Special Commands Tab Tab title The SEO Framework Title Uninstall Patch Update WP Metaboxes WP SEO (Yoast) WP SEO (Yoast) Support You have no tabs yet. Project-Id-Version: Pms72: Metabox Tabs
POT-Creation-Date: 2017-04-22 13:10+0200
PO-Revision-Date: 2017-04-22 13:12+0200
Last-Translator: Jurgen Oldenburg <jurgen@pms72.com>
Language-Team: Jurgen Oldenburg <jurgen@pms72.com>
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: ..
X-Poedit-WPHeader: pms72-metabox-tabs.php
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 ACF Opties Toon ACF velden Metabox toevoegen Nieuw tabblad Voeg SEO Framework ondersteuning toe aan Metabox tabbladen, zodat je de seo framework metabox in een tabblad kan plaatsen. Voeg WP SEO ondersteuning toe aan Metabox tabbladen, zodat je de yoast metabox in een tabblad kan plaatsen. Voeg de standaard WordPress metaboxes toe als opties in de metabox tabbladen. Voegt aanpasbare tabbladen toe aan posts en specifieke pagina’s, waar metaboxen aan toe gevoegd kunnen worden. Advanced Custom Fields Er is een fout opgetreden tijdens het patchen. Zorg er voor dat de patch map schrijfbaar is (chmod 0777). Er is een fout opgetreden tijdens het de-installeren van de plugin. Zorg er voor dat de plugin bestanden schrijfbaar zijn (chmod 0777). Patch Toepassen Auteur Reacties Aangepaste velden Discussie FOUT TIJDENS PATCHEN Extenties Algemeen Globale Opties Ga terug Terug naar overzicht Voeg ACF veld-groepen toe als optie in de tabbladen Metabox Tabbladen Metaboxen Verplaats deze metabox regel NIET GEPATCHT Geen extensies beschikbaar. ACF extensies zijn verborgen tot de ACF plugin is geactiveerd. Geen patches beschikbaar Geen OK Overschrijf de ACF optie pagina class en voeg metabox tabbladen toe. PMS 72 Metabox Tabbladen Pagina Pagina details Patch Status: Patch succesvol toegepast. Patch succesvol gedeinstalleerd. Patches Pms72: Metabox Tabbladen Revisies SEO Framework Ondersteuning Tabbladen opslaan Succesvol opgeslagen. Slug Speciale opdrachten Tab Tab titel The SEO Framework Titel Patch Deinstalleren Opslaan WP Mebaboxen WP SEO (Yoast) WP SEO (Yoast) Ondersteuning Je hebt nog geen tabbladen. 